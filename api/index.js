let express = require('express');
let bodyParser = require('body-parser');
let app = express();
let presets = require('./presets');

let api = {};

api.init = function () {
    app.use(bodyParser());
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.get('/', function (req, res) {
        res.json({ status: "online" });
    });

    app.get('/list', presets.loadList);
    app.get('/load', presets.loadPreset);
    app.post('/save', presets.savePreset);
    app.post('/new', presets.newPreset);

    app.listen(8080);
    console.log('API is online');
}

module.exports = api;