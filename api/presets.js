let fs = require('fs');
let presets = {};

presets.loadList = function (req, res) {
    let list = [];

    fs.readdirSync(`./api/presets/`).forEach(file => {
        list.push(file);
    });

    res.json(list);
}

presets.loadPreset = function (req, res) {
    let name = req.query.name;
    if (name !== undefined) {
        let path = `./api/presets/${name}.json`;
        if (fs.existsSync(path)) {
            var file = fs.readFileSync(path, 'utf8');
            let data = JSON.parse(file);

            return res.json(data);
        }
    }

    return res.status(404).send();
}

presets.savePreset = function (req, res) {
    if (!req.is('json')) {
        res.jsonp(400, { error: 'Bad request' });
        return;
    }

    let data = req.body
    let name = `./api/presets/${data.name}.json`

    fs.exists(name, function (exist) {
        if (exist) {
            console.log(`Overwriting ${name}`);
        }
        fs.writeFile(name, JSON.stringify(data), function (err) {
            if (err) throw err;
        });
    });

    res.send('ALL_OK');

}

presets.newPreset = function (req, res) {
    if (!req.is('json')) {
        res.jsonp(400, { error: 'Bad request' });
        return;
    }

    let file = Date.now();
    let filename = `${file}.json`;
    let name = `./api/presets/${filename}`;

    fs.exists(name, function (exist) {
        if (exist) {
            return;
        }

        let data = {
            name: file.toString(),
            background: "#eeeeee",
            controls: []
        };

        fs.writeFile(name, JSON.stringify(data), function (err) {
            if (err) throw err;
        });
    });

    res.send(file.toString());
}

module.exports = presets;