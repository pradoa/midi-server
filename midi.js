const midi = require('midi');
const functions = require('./functions');
const psMsg = functions.parseMessage;

const midiServer = {
    online: false,
    output: new midi.output()
};

midiServer.start = async function () {
    midiServer.online = true

    midiServer.output.openVirtualPort('NODEMIDI');

    console.log('Started out midi server');
}

midiServer.send = async function (type, args) {
    try {
        midiServer.output.sendMessage(psMsg(type, args));
    }
    catch (ex) {
        console.log(ex);
    }
}

midiServer.kill = async function () {
    midiServer.online = false;
}

module.exports = midiServer;