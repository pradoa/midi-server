const WebSocket = require('ws');
let MidiServer = null

let ws = {}

ws.start = function (ms) {
    MidiServer = ms

    const wss = new WebSocket.Server({ port: 45133 });

    wss.on('connection', function connection(ws) {
        console.log('new connection')

        ws.on('message', function incoming(message) {
            console.log('received: %s', message);

            let data = message.split(':')
            let type = data[0]
            let value = data[1]
            let control = data[2]
            let channel = data[3] - 1

            switch (type) {
                case "note":
                case "noteon": {

                    MidiServer.send('noteon', {
                        note: control,
                        velocity: value,
                        channel: channel,
                    })
                    break
                }
                case "nonote":
                case "noteoff": {

                    MidiServer.send('noteoff', {
                        note: control,
                        velocity: value,
                        channel: channel,
                    })
                    break
                }

                case "cc":
                case "control": {

                    MidiServer.send('cc', {
                        controller: control,
                        value: value,
                        channel: channel,
                    })
                    break

                }
            }
        });
    });

    console.log('Started out socket server')

}

module.exports = ws