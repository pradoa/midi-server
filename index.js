let MidiServer = require('./midi');
let SocketServer = require('./socket');
let Api = require('./api');

MidiServer.start();
SocketServer.start(MidiServer);
Api.init();

console.log('Ready for connections!');
